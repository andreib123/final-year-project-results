 
 
@startuml

(*) -->  copy preprocessed img binary into CUDA memory stream
--> execute inference asynchronously
--> copy result from CUDA memory stream to outBuffer
--> synchronise CUDA memory stream
-->(*)
@enduml
