 
 
@startuml

class DsCollector {
    -constexpr static int LANES_LEN
    -LaneBase *lanes[LANES_LEN]
    -char save_path_prefixes[LANES_LEN][PATH_MAX]
    -int imcounters[LANES_LEN]
    -cv::Rect recs[LANES_LEN]
    +DsCollector(const char* rootpath, const char* suffix)
    +void run(const char* video)
}

@enduml
