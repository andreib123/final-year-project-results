 
 
@startuml

(*) -->  Entrypoint
if "waitNFramesCounter < waitFramesAmt" then
    -->[true] "increment waitNFramesCounter by 1"
    --> "return processVehicle flag"
else
    -->[false] "clear saveSeg1ROI flag"
    --> "reset waitNFramesCounter"
    --> "set vehicleDetected flag"
    --> "increment detectedCounter"
    --> "set processVehicle flag"
    --> "return processVehicle flag"
-->(*)
@enduml
