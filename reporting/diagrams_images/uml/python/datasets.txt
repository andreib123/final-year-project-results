 
 
@startuml
(*) --> datasets

datasets --> private
datasets --> public

private --> "source (priv)" #LightSkyBlue
private --> "destination (priv)" #LimeGreen

public --> "source (pub)" #LightSkyBlue
public --> "destination (pub)" #LimeGreen

"source (priv)" --> "final-year-project-results/images/private/from_livecam/" #LightSkyBlue
"final-year-project-results/images/private/from_livecam/" --> "pickup-truck/ (priv_src)" #LightSkyBlue
"final-year-project-results/images/private/from_livecam/" --> "sedan/ (priv_src)" #LightSkyBlue
"final-year-project-results/images/private/from_livecam/" --> "suv/ (priv_src)" #LightSkyBlue
"final-year-project-results/images/private/from_livecam/" --> "van/ (priv_src)" #LightSkyBlue

"destination (priv)" --> "final-year-project-results/images/private/dataset_rescaled/" #LimeGreen
"final-year-project-results/images/private/dataset_rescaled/" --> "test (priv)" #LimeGreen
"final-year-project-results/images/private/dataset_rescaled/" --> "train (priv)" #LimeGreen
"final-year-project-results/images/private/dataset_rescaled/" --> "valid (priv)" #LimeGreen

"source (pub)" --> "final-year-project-results/images/public/cars196/" #LightSkyBlue
"final-year-project-results/images/public/cars196/" --> "pickup-truck/ (pub_src)" #LightSkyBlue
"final-year-project-results/images/public/cars196/" --> "sedan/ (pub_src)" #LightSkyBlue
"final-year-project-results/images/public/cars196/" --> "suv/ (pub_src)" #LightSkyBlue
"final-year-project-results/images/public/cars196/" --> "van/ (pub_src)" #LightSkyBlue

"destination (pub)" --> "final-year-project-results/images/public/dataset_rescaled/" #LimeGreen
"final-year-project-results/images/public/dataset_rescaled/" --> "test (pub)" #LimeGreen
"final-year-project-results/images/public/dataset_rescaled/" --> "train (pub)" #LimeGreen
"final-year-project-results/images/public/dataset_rescaled/" --> "valid (pub)" #LimeGreen

partition "For each their own" #FFFF00 {
"test (priv)" --> "pickup-truck/"
"test (priv)" --> "sedan/"
"test (priv)" --> "suv/"
"test (priv)" --> "van/"
"train (priv)" --> "pickup-truck/"
"train (priv)" --> "sedan/"
"train (priv)" --> "suv/"
"train (priv)" --> "van/"
"valid (priv)" --> "pickup-truck/"
"valid (priv)" --> "sedan/"
"valid (priv)" --> "suv/"
"valid (priv)" --> "van/"

"test (pub)" --> "pickup-truck/"
"test (pub)" --> "sedan/"
"test (pub)" --> "suv/"
"test (pub)" --> "van/"
"train (pub)" --> "pickup-truck/"
"train (pub)" --> "sedan/"
"train (pub)" --> "suv/"
"train (pub)" --> "van/"
"valid (pub)" --> "pickup-truck/"
"valid (pub)" --> "sedan/"
"valid (pub)" --> "suv/"
"valid (pub)" --> "van/"
}

@enduml
