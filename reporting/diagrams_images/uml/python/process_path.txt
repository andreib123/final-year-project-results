 
 
@startuml

partition "for each image"
(*)--> "get class name from path"
--> "encode class name as a one hot vector label, e.g. [1,0,0,0] = pickup"
--> "load image as numpy array"
--> "convert image to planar"
--> "cast image to Tensorflow Tensor object"
--> "rescale tensor values to [-1, 1] range"
--> return (image, label) Tensor
}
-->(*)
@enduml
