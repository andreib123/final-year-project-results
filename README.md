Note: this is a very large git project as it contains 4 datasets: private, private_rescaled, public, public_rescaled.
It is **5.6GB** in size.

Note: `final-year-project` and `final-year-project-results` should be in the same directory so that scripts in `final-year-project` access `final-year-project-results` paths correctly.
